require('./bootstrap');

window.Vue = require('vue').default;

import App from './components/MainApp';
import Element from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(Element);

const app = new Vue({
    el: '#app',
    render: h => h(App)
});