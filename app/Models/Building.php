<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    use HasFactory;

    public $timestamps = false;

    /**
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string $name
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeName($query, $name)
    {
        return $query->where('name', 'like', '%' . $name . '%');
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param int $priceFrom
     * @param int $priceTo
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePrice($query, $priceFrom, $priceTo)
    {
        return $query->whereBetween('price', [$priceFrom ?? 0, $priceTo ?? Building::max('price')]);
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  int $qty
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeBedrooms($query, $qty)
    {
        if( empty($qty) ) {
            return $query;
        } else {
            return $query->where('bedrooms', $qty);
        }
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param int $qty
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeBathrooms($query, $qty)
    {
        if( empty($qty) ) {
            return $query;
        } else {
            return $query->where('bathrooms', $qty);
        }
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param int $qty
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStoreys($query, $qty)
    {
        if( empty($qty) ) {
            return $query;
        } else {
            return $query->where('storeys', $qty);
        }
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param int $qty
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGarages($query, $qty)
    {
        if( empty($qty) ) {
            return $query;
        } else {
            return $query->where('garages', $qty);
        }
    }
}
