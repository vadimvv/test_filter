<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IndexBuildingRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|max:255',
            'priceFrom' => 'nullable|integer|min: 1',
            'priceTo' => 'nullable|integer|min: 1',
            'bedrooms' => 'nullable|integer|min:1|max: 255',
            'bathrooms' => 'nullable|integer|min:1|max: 255',
            'storeys' => 'nullable|integer|min:1|max: 65535',
            'garages' => 'nullable|integer|min:1|max: 255'
        ];
    }
}