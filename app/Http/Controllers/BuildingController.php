<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\IndexBuildingRequest;
use App\Models\Building;

class BuildingController extends Controller
{
    /**
     * @param \App\Http\Requests\IndexBuildingRequest $request
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(IndexBuildingRequest $request)
    {
        $buildings = Building::name($request->name)
            ->price($request->priceFrom, $request->priceTo)
            ->bedrooms($request->bedrooms)
            ->bathrooms($request->bathrooms)
            ->storeys($request->storeys)
            ->garages($request->garages)
            ->get();

        return response()->json($buildings);
    }
}
