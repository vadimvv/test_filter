<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Building;
use League\Csv\Reader;

class BuildingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $url = public_path('property-data.csv');

        $csvBuildings = Reader::createFromPath($url);

        foreach ($csvBuildings as $key => $csvBuilding)  {
            if ($key == 0) continue;

            $arr = [
                'name' => $csvBuilding[0],
                'price' => $csvBuilding[1],
                'bedrooms' => $csvBuilding[2],
                'bathrooms' => $csvBuilding[3],
                'storeys' => $csvBuilding[4],
                'garages' => $csvBuilding[5]
            ];

            Building::insert($arr);
        }
    }
}
